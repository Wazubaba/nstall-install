# N S T A L L  -  I N S T A L L
This will install a package generated via ns-build to the system.


## Building
This project uses git submodules, and should be prepped via
`git submodule init --recursive --update`, or via executing
the `update-submodules` script.

After that, simply execute `make`.

There are multiple styles of build available, and each of the main 3 can
be suffixed with 32 or 64 to specify only that architecture.

Valid rules are:
* release - build with optimizations
* debug - do not optimize whilst including debug code and gdb symbols
* test - build with unittests enabled

Sub rules are:
* package - build a collection of packages useful for distributing
* clean - clean build data and all generated files
