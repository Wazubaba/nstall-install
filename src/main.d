import std.stdio: writeln;
import std.string: format;
import std.file: exists, isDir, isFile, getcwd, write, mkdir;
import std.zlib: compress;
import std.path: asAbsolutePath, buildPath, baseName;
import std.array: array;

import dwst.argparse;
import dwst.wtar;
import nstall.io;

import common;

/*
	verify integrity of the archive
	extract to a temp folder
	pass 1: test if any files are already installed (perhaps install to /opt and use update-alternatives?)
	pass 2: install files
*/

int main(string[] args)
{
	if (!"../nstall-package.wtar.gz".IsValidArchive())
	{
		writeln("Target package appears to be corrupt, skipping");
		return -1; // TODO: perhaps allow numerous packages to be installed simultaneously?
	}

	Package pkg;

	pkg.Extract("../nstall-package.wtar.gz");

	string targ = GenerateTempDir("nstall");
	targ.mkdir();

	pkg.data.Inflate(targ);
	writeln("Wrote package to ", targ);

	// Aquire a list of the files that will be installed
	string[] files;
	foreach (leaf; pkg.data.data)
	{
		if (leaf.type == FileType.regular || leaf.type == FileType.text)
			files ~= leaf.path;
	}

	writeln(files);

	return 0;
}
